import { hasFeature } from "./helpers.js";

/* handle toggling the search fields */
const showSearch = document.querySelector('.show-search');
const searchForm = document.querySelector('.search-form');

if (showSearch && searchForm) {
  showSearch.addEventListener('click', (event) => {
    event.preventDefault();
    searchForm.classList.toggle('is-visible');
  });
}


/* add to home screen */
if('serviceWorker' in navigator) {
  navigator.serviceWorker.register('/sw.js?201912061036').then(function() {
    // console.log('Service Worker Registered');
  });
}

const addToHomeScreen = document.querySelector('.add-to-home-screen');

if (addToHomeScreen) {
  let deferredPrompt;

  window.addEventListener('beforeinstallprompt', (event) => {
    event.preventDefault();
    // Stash the event so it can be triggered later.
    deferredPrompt = event;
    // Update UI to notify the user they can add to home screen
    addToHomeScreen.style.display = 'block';

    addToHomeScreen.addEventListener('click', (e) => {
      // hide our user interface that shows our A2HS button
      addToHomeScreen.style.display = 'none';
      // Show the prompt
      deferredPrompt.prompt();
      // Wait for the user to respond to the prompt
      deferredPrompt.userChoice.then((choiceResult) => {
        if (choiceResult.outcome === 'accepted') {
          // console.log('User accepted the A2HS prompt');
        } else {
          // console.log('User dismissed the A2HS prompt');
        }
        deferredPrompt = null;
      });
    });
  });
}


/* change font sizes */
const changeFontSizeLink = document.querySelector('.change-font-size');

if (changeFontSizeLink) {
  changeFontSizeLink.addEventListener('click', () => {
    let sizeSetting = localStorage.getItem('font-size');

    switch (sizeSetting) {
      case '2px':
        localStorage.setItem('font-size', '4px');
        document.documentElement.style.setProperty('--size-adjust', '4px');
        break;
      case '4px':
        localStorage.removeItem('font-size');
        document.documentElement.style.setProperty('--size-adjust', '0px');
        break;
      default:
        localStorage.setItem('font-size', '2px');
        document.documentElement.style.setProperty('--size-adjust', '2px');
        break;
    }
  });
}
