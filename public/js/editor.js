import { init } from './pell.js';
import { hasFeature } from './helpers.js';

/* set up the message editor */
const editorBody = document.getElementById('message-editor');
const editorOutput = document.getElementById('message_body');
const backToEditor = document.getElementById('back-to-editor');

function toggleMessageEditor() {
  editorOutput.classList.toggle('is-hidden');
  editorBody.classList.toggle('is-hidden');
  backToEditor.classList.toggle('is-hidden');
}

if (editorBody && editorOutput && backToEditor) {
  const editor = init({
    element: editorBody,
    actions: [{
      name: 'bold',
      icon: `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" class="icon icon-bold" 
stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" width="16" height="16">
    <path d="M6 4h8a4 4 0 0 1 4 4 4 4 0 0 1-4 4H6z"></path>
    <path d="M6 12h9a4 4 0 0 1 4 4 4 4 0 0 1-4 4H6z"></path>
</svg>`,
    }, {
      name: 'italic',
      icon: `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" class="icon icon-italic" 
stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" width="16" height="16">
    <path d="M19 4h-9M14 20H5M14.7 4.7L9.2 19.4"/>
</svg>`,
    }, {
      name: 'link',
      icon: `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" class="icon icon-link" 
stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" width="16" height="16">
    <path d="M10 13a5 5 0 0 0 7.54.54l3-3a5 5 0 0 0-7.07-7.07l-1.72 1.71"></path>
    <path d="M14 11a5 5 0 0 0-7.54-.54l-3 3a5 5 0 0 0 7.07 7.07l1.71-1.71"></path>
</svg>`,
    }, {
      name: 'image',
      icon: `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" class="icon icon-image" 
stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" width="16" height="16">
    <rect x="3" y="3" width="18" height="18" rx="2"/>
    <circle cx="8.5" cy="8.5" r="1.5"/>
    <path d="M20.4 14.5L16 10 4 20"/>
</svg>`,
    }, {
      name: 'html',
      title: 'Edit HTML',
      icon: `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" class="icon icon-html" 
stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" width="16" height="16">
    <polyline points="16 18 22 12 16 6"></polyline>
    <polyline points="8 6 2 12 8 18"></polyline>
</svg>&nbsp;HTML`,
      result: () => toggleMessageEditor()
    }],
    defaultParagraphSeparator: 'p',
    onChange: html => {
      editorOutput.textContent = html;
    }
  });

  if (editorOutput.value) {
    editor.content.innerHTML = editorOutput.value;
  }

  const backToEditorButton = backToEditor.querySelector('button');

  if (backToEditorButton) {
    backToEditorButton.addEventListener('click', (event) => {
      event.preventDefault();
      editor.content.innerHTML = editorOutput.value;
      toggleMessageEditor();
    });
  }
}


/* show a preview of the image to be uploaded */
const messageForm = document.querySelector('form.message-form');
const photoFormGroup = document.querySelector('.photo-form-group');
const imageFileNameInput = document.querySelector('.image-file-name');
const removeImageButton = document.querySelector('.remove-image');
const uploadedImageArea = document.querySelector('.uploaded-image');
const imagePath = document.querySelector('.image-path');

if (photoFormGroup && imageFileNameInput && removeImageButton && uploadedImageArea && imagePath) {
  // load a preview when a file is selected
  imageFileNameInput.addEventListener('change', (event) => {
    // resize it
    // rotate it
    // append it to a new input
    // clear the old input
    // show an image of the file
    let input = event.target;

    if (input.files && input.files[0]) {
      const reader = new FileReader();

      reader.onload = (loadEvent) => {
        // resize it
        let image = new Image();
        let canvas = document.createElement('canvas');
        uploadedImageArea.querySelector('img').src = loadEvent.target.result;
        uploadedImageArea.classList.remove('is-hidden');
        photoFormGroup.classList.add('is-hidden');
      };

      reader.readAsDataURL(input.files[0]);
    }
  });

  // reset the file upload buttons when image is removed
  removeImageButton.addEventListener('click', (event) => {
    event.preventDefault();
    photoFormGroup.classList.remove('is-hidden');
    uploadedImageArea.classList.add('is-hidden');
    if (hasFeature('SHOW_IMAGE_PREVIEW')) {
      imagePath.value = '';
    }
  })
}


/* save message */
const saveMessageButton = document.querySelector('.message-save');
const formFields = document.querySelector('.message-form .form-fields');

if (hasFeature("SAVE_WITH_AJAX") && saveMessageButton && uploadedImageArea && formFields) {
  saveMessageButton.addEventListener('click', (event) => {
    // event.preventDefault();
    console.log(event);
  });
}


// yet to figure the resizing out...
function resizeImage(settings) {
  let file = settings.file;
  let maxSize = settings.maxSize;
  let reader = new FileReader();
  let image = new Image();
  let canvas = document.createElement('canvas');

  function dataURItoBlob(dataURI) {
    let bytes = dataURI.split(',')[0].indexOf('base64') >= 0 ?
      atob(dataURI.split(',')[1]) :
      unescape(dataURI.split(',')[1]);
    let mime = dataURI.split(',')[0].split(':')[1].split(';')[0];
    let max = bytes.length;
    let ia = new Uint8Array(max);
    for (let i = 0; i < max; i++)
      ia[i] = bytes.charCodeAt(i);
    return new Blob([ia], { type: mime });
  }

  function resize() {
    let width = image.width;
    let height = image.height;
    if (width > height) {
      if (width > maxSize) {
        height *= maxSize / width;
        width = maxSize;
      }
    } else {
      if (height > maxSize) {
        width *= maxSize / height;
        height = maxSize;
      }
    }
    canvas.width = width;
    canvas.height = height;
    canvas.getContext('2d').drawImage(image, 0, 0, width, height);
    let dataUrl = canvas.toDataURL('image/jpeg');
    return dataURItoBlob(dataUrl);
  }

  return new Promise(function (ok, no) {
    if (!file.type.match(/image.*/)) {
      no(new Error("Not an image"));
      return;
    }
    reader.onload = function (readerEvent) {
      image.onload = function () { return ok(resize()); };
      image.src = readerEvent.target.result;
    };

    reader.readAsDataURL(file);
  });
}
