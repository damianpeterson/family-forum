/**
 * Create an element with attributes and contents
 * =============================================================================
 * Supply the name of the element i.e. 'div' or 'input', etc. Defaults to 'div'.
 * Add any element attributes ('className', not 'class') you can include event
 * listeners within an 'on' attribute.
 * Any internal content can be supplied in subsequent n parameters.
 * Make sure you include an `xmlns` attribute on any SVG element or sub-element.
 * -----------------------------------------------------------------------------
 *
 * Example:
 * createElement(
 *   'div',                              <- the type of element to create
 *   {
 *     className: 'form-control',        <- give it a css class or classes
 *     style: {
 *       marginTop: '20px'               <- custom styles must be JS syntax
 *     },
 *     data: {
 *       productId: 123                  <- data attributes must be camelCase
 *     }
 *   },
 *   'Hello ',                           <- add a plain text inside it
 *   createElement(                      <- even add another createElement()...
 *     'input',                          <- ...which is an input...
 *     {
 *       type: 'checkbox',               <- ...of type checkbox
 *       on: {
 *         change: () => {               <- events go inside an 'on' attribute
 *           console.log('changed');
 *         }
 *       }
 *     }
 *   ),
 *   ' world'                            <- more plain text content appended
 * );
 *
 * Outputs:
 * <div class="form-control" style="margin-top: 20px" data-product-id="123">
 *     Hello <input type="checkbox"> world
 * </div>
 * The checkbox has a change event handler attached to it.
 * -----------------------------------------------------------------------------
 */
export default function createElement(element = 'div', attributes = {}, ...contents) {
    let newElement = attributes.hasOwnProperty('xmlns') ?
        document.createElementNS(attributes.xmlns, element) :
        document.createElement(element);

    if (attributes) {
        Object.keys(attributes).forEach((key) => {
            if (key === 'on') {
                Object.keys(attributes[key]).forEach(eventListenerType => {
                    newElement.addEventListener(eventListenerType, attributes[key][eventListenerType]);
                });
            } else if (key === 'data') {
                Object.keys(attributes[key]).forEach(dataType => {
                    newElement.dataset[dataType] = attributes[key][dataType];
                });
            } else if (key === 'style') {
                Object.keys(attributes[key]).forEach(styleType => {
                    newElement.style[styleType] = attributes[key][styleType];
                });
            } else if (['className'].includes(key)) {
                newElement[key] = attributes[key];
            } else {
                newElement.setAttribute(key, attributes[key]);
            }
        });
    }

    contents.forEach(content => {
        if (typeof content === 'object' && content instanceof Element) {
            newElement.appendChild(content);
        } else if (typeof content === 'string' || typeof content === 'number') {
            let textElement = document.createTextNode(content);
            newElement.appendChild(textElement);
        }
    });

    return newElement;
}
