/**
 * Check to see whether a feature has been enabled at the root level
 *
 * @param name
 * @returns {*}
 */
export function hasFeature(name) {
  if (typeof features === "object" && Object.keys(features).includes(name)) {
    return features[name];
  }

  return false;
}
