import { hasFeature } from './helpers.js';

const messages = document.querySelectorAll('.message');

/* handle like clicks */
const likeLinks = document.querySelectorAll('.like');

function toggleLike(likeLink) {
  fetch(likeLink.href, {method: 'POST'}).then((response) => {
    return response.json();
  }).then((json) => {
    likeLink.classList.toggle('liked', json.liked === true);
    likeLink.title = 'Like this';
    if (json.likedBy) {
      likeLink.title = `Liked by ${json.likedBy}`;
    }
    let likeCount = likeLink.querySelector('.like-count');

    if (likeCount && json.count === 0) {
      likeCount.remove();
    } else {
      if (!likeCount) {
        likeCount = document.createElement('span');
        likeCount.classList.add('like-count');
        likeLink.appendChild(likeCount);
      }

      likeCount.textContent = json.count;
    }
  });
}

[...likeLinks].forEach((likeLink) => {
  likeLink.addEventListener('click', (event) => {
    event.preventDefault();

    if (likeLink.classList.contains('my-likes') || likeLink.classList.contains('liked')) {
      const messageLikes = likeLink.closest('.message-likes');
      if (messageLikes.likesInfo) {
        messageLikes.likesInfo.remove();
        delete messageLikes.likesInfo;
      } else {
        let likesInfo = document.createElement('div');
        let likesInfoText = document.createElement('p');
        likesInfoText.textContent = likeLink.title;
        likesInfo.appendChild(likesInfoText);
        likesInfo.className = 'message-likes-info';

        if (likeLink.classList.contains('liked')) {
          let undoLike = document.createElement('a');
          undoLike.textContent = 'Remove my like';
          undoLike.href = '#';

          undoLike.addEventListener('click', (event) => {
            event.preventDefault();
            toggleLike(likeLink);
            messageLikes.likesInfo.remove();
            delete messageLikes.likesInfo;
          });

          likesInfo.appendChild(undoLike);
        }

        messageLikes.appendChild(likesInfo);
        messageLikes.likesInfo = likesInfo;
      }
    } else {
      toggleLike(likeLink);
    }
  });
});


/* replace youtube links */
[...messages].forEach((message) => {
  const messageBody = message.querySelector('.message-body');
  const matchPattern = new RegExp(/youtu(?:\.be|be\.com)\/(?:.*v(?:\/|=)|(?:.*\/)?)([a-zA-Z0-9-_]+)/);

  if (messageBody) {
    let matches = messageBody.innerText.match(matchPattern);

    if (matches) {
      let embeddedVideoWrapper = document.createElement('div');
      embeddedVideoWrapper.classList.add('embedded-video', 'has-preview');

      let embeddedVideoThumbnail = document.createElement('img');
      embeddedVideoThumbnail.src = `https://img.youtube.com/vi/${matches[1]}/mqdefault.jpg`;

      embeddedVideoThumbnail.addEventListener('click', () => {
        let embeddedVideo = document.createElement('iframe');
        embeddedVideo.src = `https://www.youtube-nocookie.com/embed/${matches[1]}`;
        embeddedVideo.setAttribute('frameborder', '0');
        embeddedVideo.setAttribute('allowfullscreen', '');
        embeddedVideo.setAttribute('allow', 'autoplay');
        embeddedVideo.setAttribute('width', '100%');
        embeddedVideo.setAttribute('height', (messageBody.clientWidth * (9 / 16)).toString());
        embeddedVideoWrapper.appendChild(embeddedVideo);
        embeddedVideoWrapper.classList.remove('has-preview');
        embeddedVideoThumbnail.remove();
      });

      embeddedVideoWrapper.appendChild(embeddedVideoThumbnail);

      messageBody.appendChild(embeddedVideoWrapper);
    }
  }
});
