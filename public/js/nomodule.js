var body = document.querySelector('body');
var warningMessage = document.createElement('div');
warningMessage.textContent = 'Hi, Damian here. It looks like the browser you\'re using might not work properly with ' +
  'this version of the website. If you\'re seeing this message get in contact with me so I can fix it and in the ' +
  'meantime you should still be able to use the ';
var oldLink = document.createElement('a');
oldLink.href = 'https://old-family.peterson.nz/';
oldLink.textContent = 'older version of the forum';
oldLink.style.color = 'white';
oldLink.style.textDecoration = 'underline';
oldLink.style.fontWeight = 'bold';
warningMessage.appendChild(oldLink);

body.style.gridTemplateRows = '[warning-start] auto [warning-end header-start] auto [header-end content-start] 1fr [content-end footer-start] auto [footer-end]';
warningMessage.style.gridRowStart = 'warning-start';
warningMessage.style.gridColumnStart = 'page-start';
warningMessage.style.gridColumnEnd = 'page-end';
warningMessage.style.padding = '20px';
warningMessage.style.backgroundColor = 'darkred';
warningMessage.style.color = 'white';

body.insertBefore(warningMessage, body.firstChild);
