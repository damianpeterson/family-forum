export function hasUpperCase(string) {
  return (string.match(/[A-Z]/) !== null);
}

export function hasLowerCase(string) {
  return (string.match(/[a-z]/) !== null);
}

export function hasNumber(string) {
  return (string.match(/[0-9]/) !== null);
}

export function hasOther(string) {
  return (string.match(/[^0-9A-Za-z]/) !== null);
}

/**
 * PASSWORD STRENGTH INDICATOR
 * Display an indication of the strength of the password as it is being typed.
 */
export const defaultOptions = {
  passwordActiveClass: 'input-has-strength',
  strengthFlagInvalid: 'invalid',
  strengthFlagPoor: 'poor',
  strengthFlagOk: 'ok',
  strengthFlagGood: 'good',
  scoreOk: 4,
  scoreGood: 9,
  messageInvalid: 'Please make your password longer.',
  messagePoor: 'This is easy to guess.',
  messagePoorSuggestionDefault: 'Try making it longer.',
  messagePoorSuggestionNoNumbers: 'Try adding a number.',
  messagePoorSuggestionNoUppercase: 'Try adding an uppercase letter.',
  messageOk: 'Good password. Difficult to guess.',
  messageGood: 'Excellent password! Very hard to guess.'
};

export default class PasswordStrengthIndicator {
  constructor(
    strengthHelper = null,
    options = {}
  ) {
    this.options = Object.assign({}, defaultOptions, options);

    if (strengthHelper !== null) {
      return { code: this.init(strengthHelper) };
    }
  }

  /**
   * Attach a strength helper to its target password input and update it as the password changes
   *
   * @param strengthHelper
   */
  init(strengthHelper) {
    if (!strengthHelper) {
      return 404;
    }

    const passwordInput = document.getElementById(strengthHelper.dataset.target);

    if (!passwordInput || passwordInput.classList.contains(this.options.passwordActiveClass)) {
      return 404;
    }

    passwordInput.classList.add(this.options.passwordActiveClass);
    strengthHelper.classList.remove('hide');

    passwordInput.addEventListener('input', () => {
      this.updateStrength(passwordInput, strengthHelper);
    });

    return 0;
  }

  /**
   * Calculate the score of a password based on the length and variety of characters used.
   *
   * @param password
   * @param minLength
   * @returns {number}
   */
  static calculateScore(password, minLength = 8) {
    const numberOfCharacters = password.length;

    return Math.min(
      Math.min(10, numberOfCharacters),
      (
        Math.max(numberOfCharacters - minLength, 0)
        + (hasUpperCase(password) ? 2 : 0)
        + (hasLowerCase(password) ? 2 : 0)
        + (hasNumber(password) ? 2 : 0)
        + (hasOther(password) ? 3 : 0)
        + ((hasUpperCase(password) && hasLowerCase(password) && hasNumber(password) && hasOther(password))
          ? 4 : 0)
      )
    );
  }

  /**
   * Get the message of the strength helper
   *
   * @param dataset
   * @param score
   * @param password
   * @param minLength
   */
  getStrengthMessage(dataset, score, password, minLength) {
    let strengthMessage = dataset.messageGood || this.options.messageGood;

    if (password.length < minLength) {
      strengthMessage = dataset.messageInvalid || this.options.messageInvalid;
    } else if (score <= this.options.scoreOk) {
      let messagePoor = dataset.messagePoor || this.options.messagePoor;
      let suggestion = dataset.messagePoorSuggestionDefault || this.options.messagePoorSuggestionDefault;
      if (password.length > minLength) {
        if (!hasNumber(password)) {
          suggestion = dataset.messagePoorSuggestionNoNumbers || this.options.messagePoorSuggestionNoNumbers;
        } else if (!hasUpperCase(password)) {
          suggestion = dataset.messagePoorSuggestionNoUppercase || this.options.messagePoorSuggestionNoUppercase;
        }
      }
      strengthMessage = `${messagePoor} ${suggestion}`;
    } else if (score <= this.options.scoreGood) {
      strengthMessage = dataset.messageOk || this.options.messageOk;
    }

    return strengthMessage;
  }

  /**
   * Get strength flag of the strength helper
   *
   * @param dataset
   * @param score
   * @param password
   * @param minLength
   */
  getStrengthFlag(dataset, score, password, minLength) {
    let strengthFlag = dataset.strengthFlagGood || this.options.strengthFlagGood;

    if (password.length < minLength) {
      strengthFlag = dataset.strengthFlagInvalid || this.options.strengthFlagInvalid;
    } else if (score <= this.options.scoreGood) {
      strengthFlag = dataset.strengthFlagOk || this.options.strengthFlagOk;
    }

    return strengthFlag;
  }

  /**
   * Calculate the strength of a password and update the strength helper accordingly
   *
   * @param passwordInput
   * @param strengthHelper
   * @param topPasswords
   * @param hashedPasswords
   */
  updateStrength(passwordInput, strengthHelper, topPasswords, hashedPasswords) {
    const password = passwordInput.value;
    const minLength = passwordInput.minLength || 8;

    const score = PasswordStrengthIndicator.calculateScore(password, minLength);

    strengthHelper.dataset.score = score.toString();
    strengthHelper.dataset.strength = this.getStrengthFlag(strengthHelper.dataset, score, password, minLength);
    strengthHelper.textContent = this.getStrengthMessage(strengthHelper.dataset, score, password, minLength);
  }
}
