/**
 * Attach a show/hide toggler to a password field to switch between password and plain text.
 *
 * If you supply a `hide` class to the toggler element it will be removed on init. Once initialised, it'll attach a
 * class of `has-toggler` to the password input.
 *
 * e.g.
 * <input type="password" id="my-password" />
 * <span class="js-password-toggler hide" data-target="my-password">show</span>
 *
 * const toggler = document.querySelector('.js-password-toggler');
 * const result = new PasswordToggler(toggler);
 * console.log(result); // { code: 0 }
 */
export default class PasswordToggler {
  constructor(
    toggler = null,
    isToggledClass = 'password-toggler-is-active',
    hasTogglerClass = 'input-has-password-toggler'
  ) {
    this.isToggledClass = isToggledClass;
    this.hasTogglerClass = hasTogglerClass;

    if (toggler !== null) {
      return { code: this.init(toggler) };
    }
  }

  handleToggleClick(toggler, passwordInput) {
    if (toggler.classList.contains(this.isToggledClass)) {
      toggler.classList.remove(this.isToggledClass);
      toggler.textContent = 'show';
      passwordInput.type = 'password';
    } else {
      toggler.classList.add(this.isToggledClass);
      toggler.textContent = 'hide';
      passwordInput.type = 'text';
    }

    passwordInput.focus();
  }

  init(toggler) {
    if (!toggler) {
      return 404;
    }

    let passwordInput = document.getElementById(toggler.dataset.target);

    if (!passwordInput) {
      return 404;
    }

    passwordInput.classList.add(this.hasTogglerClass);
    toggler.classList.remove('hide');
    toggler.addEventListener('click', () => {
      this.handleToggleClick(toggler, passwordInput);
    });

    return 0;
  }
}
