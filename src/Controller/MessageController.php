<?php

namespace App\Controller;

use App\Entity\Message;
use App\Entity\User;
use App\Entity\UserMessageLike;
use App\Form\MessageType;
use App\Service\UpdateMessageLikeCountService;
use App\Service\UpdateUserMessageCountService;
use Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class MessageController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     * @Route("/messages", name="messages")
     * @Route("/messages/{page}", name="messages_by_page", requirements={"page" = "\d+"})
     * @param int $page
     * @return Response
     * @throws Exception
     */
    public function homepage($page = 1)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $messageRepository = $entityManager->getRepository(Message::class);
        $userRepository = $entityManager->getRepository(User::class);

        $messagesCount = $messageRepository->getMessageCount();
        $pagesCount = ceil($messagesCount / Message::MESSAGES_PER_PAGE);

        $messages = $messageRepository->findBy(
            ['deletedAt' => null],
            ['createdAt' => 'DESC'],
            Message::MESSAGES_PER_PAGE,
            Message::MESSAGES_PER_PAGE * ($page - 1)
        );

        $hasBirthday = $userRepository->getHasBirthday();

        $user = $this->getUser();
        $user->setVisitedAt(new \DateTime('now'));
        $entityManager->persist($user);
        $entityManager->flush();

        return $this->render(
            'default/messages.html.twig',
            [
                'messages' => $messages,
                'messageCount' => $messagesCount,
                'page' => $page,
                'pagesCount' => $pagesCount,
                'prevLink' => $this->generateUrl(
                    'messages_by_page',
                    ['page' => $page - 1]
                ),
                'nextLink' => $this->generateUrl(
                    'messages_by_page',
                    ['page' => $page + 1]
                ),
                'context' => 'messages',
                'hasBirthday' => $hasBirthday
            ]
        );
    }

    /**
     * @Route("/rss", name="rss")
     * @return Response
     * @throws Exception
     */
    public function rss()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $messageRepository = $entityManager->getRepository(Message::class);

        $messages = $messageRepository->findBy(
            ['deletedAt' => null],
            ['createdAt' => 'DESC'],
            50
        );

        $user = $this->getUser();
        $user->setVisitedAt(new \DateTime('now'));
        $entityManager->persist($user);
        $entityManager->flush();

        $response = new Response($this->renderView('default/messages.xml.twig', ['messages' => $messages]));
        $response->headers->set('Content-Type', 'text/xml; charset=utf-8');
        return $response;
    }

    /**
     * @Route("/messages/person/{id}", name="messages_by_person", requirements={"id" = "\d+"})
     * @Route("/messages/person/{id}/{page}", name="messages_by_person_by_page", requirements={"id" = "\d+", "page" = "\d+"})
     * @param User $user
     * @param int $page
     * @return Response
     * @throws Exception
     */
    public function userMessages(User $user, $page = 1)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $messageRepository = $entityManager->getRepository(Message::class);

        $messagesCount = $user->getMessageCount();
        $pagesCount = ceil($messagesCount / Message::MESSAGES_PER_PAGE);

        $messages = $messageRepository->findBy(
            [
                'deletedAt' => null,
                'user' => $user
            ],
            ['createdAt' => 'DESC'],
            Message::MESSAGES_PER_PAGE,
            Message::MESSAGES_PER_PAGE * ($page - 1)
        );

        $currentUser = $this->getUser();
        $currentUser->setVisitedAt(new \DateTime('now'));
        $entityManager->persist($currentUser);
        $entityManager->flush();

        return $this->render(
            'default/messages.html.twig',
            [
                'messages' => $messages,
                'messageCount' => $messagesCount,
                'page' => $page,
                'pagesCount' => $pagesCount,
                'prevLink' => $this->generateUrl(
                    'messages_by_person_by_page',
                    ['id' => $user->getId(), 'page' => $page - 1]
                ),
                'nextLink' => $this->generateUrl(
                    'messages_by_person_by_page',
                    ['id' => $user->getId(), 'page' => $page + 1]
                ),
                'context' => 'user-messages'
            ]
        );
    }

    /**
     * @Route("/message/{id}/context", name="message_context")
     * @param Message $message
     * @return Response
     * @throws Exception
     */
    public function messageContext(Message $message)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $messageRepository = $entityManager->getRepository(Message::class);

        $page = max(
            1,
            ceil($messageRepository->getMessageCountNewerThanMessage($message) / Message::MESSAGES_PER_PAGE)
        );

        return $this->redirect(
            $this->generateUrl('messages_by_page', ['page' => $page]) . '#message-' . $message->getId()
        );
    }

    /**
     * @Route("/message/{id}/check-for-new", name="message_check_for_new")
     * @param Message $message
     * @return JsonResponse
     */
    public function checkForNewMessages(Message $message)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $messageRepository = $entityManager->getRepository(Message::class);

        $newMessages = $messageRepository->getMessageCountNewerThanMessage($message);

        return new JsonResponse(['result' => $newMessages]);
    }

    /**
     * @param Request $request
     * @param UpdateUserMessageCountService $updateUserMessageCountService
     * @return Response
     * @throws Exception
     * @Route("/message/create", name="message_create")
     */
    public function messageCreate(Request $request, UpdateUserMessageCountService $updateUserMessageCountService)
    {
        $user = $this->getUser();

        $entityManager = $this->getDoctrine()->getManager();
        $messageRepository = $entityManager->getRepository(Message::class);

        $messages = $messageRepository->findBy(
            ['deletedAt' => null],
            ['createdAt' => 'DESC'],
            Message::MESSAGES_PER_PAGE
        );

        $message = new Message();

        $form = $this->createMessageForm($message);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $message->upload();
            $message->setCreatedAt(new \DateTime('now'));
            $message->setUpdatedAt(new \DateTime('now'));
            $message->setUser($user);

            $entityManager->persist($message);
            $entityManager->flush();

            $updateUserMessageCountService->updateUser($user);

            return $this->redirectToRoute('homepage');
        }

        return $this->render(
            'default/messageCreate.html.twig',
            [
                'form' => $form->createView(),
                'message' => $message,
                'messages' => $messages,
                'showDelete' => false
            ]
        );
    }

    /**
     * @param Request $request
     * @param Message $message
     * @return Response
     * @throws Exception
     * @Route("/message/{id}/update", name="message_update")
     * @Security("user == message.getUser()")
     */
    public function messageUpdate(Request $request, Message $message)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $form = $this->createMessageForm(
            $message,
            $this->generateUrl('message_update', ['id' => $message->getId()])
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $message->upload();
            $message->setUpdatedAt(new \DateTime('now'));

            $entityManager->persist($message);
            $entityManager->flush();

            return $this->redirectToRoute('homepage');
        }

        return $this->render(
            'default/messageEdit.html.twig',
            [
                'form' => $form->createView(),
                'message' => $message
            ]
        );
    }

    /**
     * @Route("/message/{id}/edit", name="message_edit")
     * @param Message $message
     * @return Response
     * @throws Exception
     * @Security("user == message.getUser()")
     */
    public function messageEdit(Message $message)
    {
        $form = $this->createMessageForm(
            $message,
            $this->generateUrl('message_update', ['id' => $message->getId()])
        );

        return $this->render(
            'default/messageEdit.html.twig',
            [
                'form' => $form->createView(),
                'message' => $message
            ]
        );
    }

    /**
     * @Route("/message/{id}/delete", name="message_delete")
     * @param Message $message
     * @param UpdateUserMessageCountService $updateUserMessageCountService
     * @return Response
     * @throws Exception
     * @Security("user == message.getUser()")
     */
    public function messageDelete(Message $message, UpdateUserMessageCountService $updateUserMessageCountService)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $message->setDeletedAt(new \DateTime('now'));
        $entityManager->persist($message);
        $entityManager->flush();

        $updateUserMessageCountService->updateUser($this->getUser());

        return $this->redirect($this->generateUrl('homepage'));
    }

    private function createMessageForm($message = null, $action = null)
    {
        if (!$action) {
            $action = '#';
        }

        $user = $this->getUser();

        $form = $this->createForm(
            MessageType::class,
            $message,
            [
                'action' => $action,
                'method' => 'POST',
                'attr' => [
                    'role' => 'form'
                ]
            ]
        );

        return $form;
    }

    /**
     * @Route(
     *     "/message/{id}/like",
     *     name="like_message",
     *     requirements={"id" = "\d+"},
     *     methods={"POST"}
     * )
     * @param Message $message
     * @param UpdateMessageLikeCountService $updateMessageLikeCountService
     * @return JsonResponse
     * @throws Exception
     */
    public function likeMessage(
        Message $message,
        UpdateMessageLikeCountService $updateMessageLikeCountService
    )
    {
        $user = $this->getUser();

        $entityManager = $this->getDoctrine()->getManager();

        $userMessageLikeRepository = $entityManager->getRepository(UserMessageLike::class);

        $like = $userMessageLikeRepository->findOneBy(['user' => $user, 'message' => $message]);
        $liked = false;

        if ($like) {
            $entityManager->remove($like);
        } else {
            $like = new UserMessageLike();
            $like->setUser($user);
            $like->setMessage($message);
            $like->setCreatedAt(new \DateTime('now'));
            $entityManager->persist($like);
            $liked = true;
        }

        $entityManager->flush();

        $updateMessageLikeCountService->updateMessage($message);

        return new JsonResponse([
            'result' => 1,
            'liked' => $liked,
            'count' => $message->getLikeCount(),
            'likedBy' => $message->getUserLikesNames()
        ]);
    }

    /**
     * @Route("/messages/all", name="messages_all")
     * @return Response
     */
    public function allMessages()
    {
        $entityManager = $this->getDoctrine()->getManager();

        $messageRepository = $entityManager->getRepository(Message::class);
        $messages = $messageRepository->getAllMessagesZoomed();

        $userRepository = $entityManager->getRepository(User::class);
        $users = $userRepository->getUsersByNextBirthday();

        $sortedMessages = [];

        foreach ($messages as $message) {
            $sortedMessages[$message['created_at']->format('Y')][$message['created_at']->format('F')][] = [
                'i' => $message['message_id'],
                'u' => $message['user_id'],
            ];
        }

        return $this->render(
            'default/messagesAll.html.twig',
            [
                'sortedMessages' => json_encode($sortedMessages),
                'totalMessages' => count($messages),
                'users' => $users,
                'context' => 'messages-all'
            ]
        );
    }

    /**
     * Search the site
     *
     * @Route("/search", name="search")
     * @Route("/search/{page}", name="search_by_page", requirements={"page" = "\d+"})
     * @param Request $request
     * @param int $page
     * @return RedirectResponse|Response
     */
    public function search(Request $request, $page = 1)
    {
        $searchTerm = $request->get('searchTerm');

        if (empty($searchTerm) || strlen($searchTerm) < 2) {
            return $this->redirect($request->headers->get('referer'));
        }

        $entityManager = $this->getDoctrine()->getManager();

        $messageRepository = $entityManager->getRepository(Message::class);
        $messages = $messageRepository->getMessagesBySearchTerm($searchTerm, $page);

        $messagesCount = $messageRepository->getMessagesBySearchTermCount($searchTerm, $page);
        $pagesCount = ceil($messagesCount / Message::MESSAGES_PER_PAGE);

        $userRepository = $entityManager->getRepository(User::class);
        $users = $userRepository->getUsersByNextBirthday();

        return $this->render(
            'default/messages.html.twig',
            array(
                'searchTerm' => $searchTerm,
                'messages' => $messages,
                'users' => $users,
                'prevLink' => $this->generateUrl(
                    'search_by_page',
                    [
                        'page' => $page - 1,
                        'searchTerm' => $searchTerm
                    ]
                ),
                'nextLink' => $this->generateUrl(
                    'search_by_page',
                    [
                        'page' => $page + 1,
                        'searchTerm' => $searchTerm
                    ]
                ),
                'page' => $page,
                'pagesCount' => $pagesCount,
                'context' => 'search'
            )
        );
    }
}
