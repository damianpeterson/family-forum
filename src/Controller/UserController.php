<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class UserController extends AbstractController
{
    /**
     * @Route("/people", name="users")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showUsers(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $userRepository = $entityManager->getRepository(User::class);

        $users = $userRepository->getUsersByNextBirthday();

        return $this->render(
            'default/users.html.twig',
            [
                'users' => $users
            ]
        );
    }

    /**
     * @Route("/people/{id}/edit", name="user_edit", requirements={"id" = "\d+"})
     * @param Request $request
     * @param User $userToEdit
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     * @Security("user == userToEdit")
     */
    public function editUser(Request $request, User $userToEdit)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $form = $this->createUserForm($userToEdit);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $newPassword = $form['password']->getData();

            if ($newPassword) {
                $userToEdit->setPassword(password_hash($newPassword, PASSWORD_BCRYPT, ['cost' => 13]));
            }

            $userToEdit->setUpdatedAt(new \DateTime('now'));

            $entityManager->persist($userToEdit);
            $entityManager->flush();

            return $this->redirect($this->generateUrl('homepage'));
        }

        return $this->render(
            'default/userEdit.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    private function createUserForm(User $user)
    {
        $form = $this->createForm(
            UserType::class,
            $user,
            [
                'action'    => $this->generateUrl('user_edit', ['id' => $user->getId()]),
                'method'    => 'POST',
                'attr'      => [
                    'role' => 'form'
                ]
            ]
        );

        return $form;
    }
}
