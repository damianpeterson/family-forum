<?php

namespace App\DQL;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;

class DateFormatFunction extends FunctionNode
{
    public $dateExpression = null;
    public $patternExpression = null;

    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->patternExpression = $parser->StringPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->dateExpression = $parser->ArithmeticExpression();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        return 'strftime(' .
            $this->patternExpression->dispatch($sqlWalker) . ', ' .
            $this->dateExpression->dispatch($sqlWalker) .
            ')';
    }
}

