<?php

namespace App\Entity;

use App\Entity\Common\CreatableEntity;
use App\Entity\Common\DeletableEntity;
use App\Entity\Common\IdentifiableEntity;
use App\Entity\Common\UpdatableEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Message
 *
 * @ORM\Table(name="messages")
 * @ORM\Entity(repositoryClass="App\Repository\MessageRepository")
 */
class Message
{
    use IdentifiableEntity;
    use CreatableEntity;
    use UpdatableEntity;
    use DeletableEntity;

    const MESSAGES_PER_PAGE = 30;
    const MAX_IMAGE_SIZE = 1000;

    /**
     * @var User $user
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="messages")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text", nullable=true)
     */
    private $body;

    /**
     * @Assert\File(maxSize="100M")
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(name="file_path", type="string", length=255, nullable=true)
     */
    private $filePath;

    /**
     * @var UserMessageLike[] | ArrayCollection $userLikes
     *
     * @ORM\OneToMany(targetEntity="UserMessageLike", mappedBy="message")
     */
    private $userLikes;

    /**
     * @var string $likeCount
     *
     * @ORM\Column(name="like_count", type="integer", nullable=true)
     */
    private $likeCount;

    public function __construct()
    {
        $this->userLikes = new ArrayCollection();
    }

    /**
     * Set user
     *
     * @param User $user
     * @return self
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return self
     */
    public function setBody($body)
    {
        $icons = [
            ':)'        => '☺️',
            ':-)'       => '☺️',
            ':('        => '😢',
            ':-('       => '😢',
            ';)'        => '😉',
            ';-)'       => '😉',
            ':D'        => '😀',
            ':*'        => '😘',
            ':X'        => '😘',
            ':x'        => '😘',
            '**---____' => '😏',
            '***---___' => '😏',
            '*-_'       => '😏',
            '**--__'    => '😏',
        ];

        // not using these anymore
        $longListOfIcons = [
            ':-)'       => '☺️',
            ':-('       => '😢',
            ';-)'       => '😉',
            ':-D'       => '😀',
            '8-O'       => '😳',
            ':-O'       => '😳',
            ':-0'       => '😳',
            '8-)'       => '😎',
            ':-?'       => '😕',
            ':-*'       => '😘',
            ':-P'       => '😛',
            ':-|'       => '😐',
            '}-('       => '😠',
            ':)'        => '☺️',
            ':('        => '😢',
            ';)'        => '😉',
            ':D'        => '😀',
            '8O'        => '😳',
            '8)'        => '😎',
            ':?'        => '😕',
            ':*'        => '😘',
            ':X'        => '😘',
            ':x'        => '😘',
            ':P'        => '😛',
            ';P'        => '😛',
            ':p'        => '😛',
            ';p'        => '😛',
            ':|'        => '😐',
            '}|'        => '😠',
            '**---____' => '😏',
            '***---___' => '😏',
            '*-_'       => '😏',
            '**--__'    => '😏',
        ];

        foreach ($icons as $key => $value) {
            $body = str_replace($key, $value, $body);
        }

        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string 
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set filePath
     *
     * @param string $filePath
     * @return self
     */
    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;

        return $this;
    }

    /**
     * Get filePath
     *
     * @return string
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * Get all user likes for this message
     *
     * @return UserMessageLike[]|ArrayCollection
     */
    public function getUserLikes()
    {
        return $this->userLikes;
    }

    public function getUserLikesNames()
    {
        return join(", ", array_map(function($userLike) {
            return $userLike->getUser()->getUsername();
        }, $this->userLikes->toArray()));
    }

    /**
     * Add a user like for this message
     *
     * @param UserMessageLike $userMessageLike
     * @return self
     */
    public function addUserLike(UserMessageLike $userMessageLike)
    {
        $this->userLikes[] = $userMessageLike;

        return $this;
    }

    /**
     * Remove a user like for a message
     *
     * @param UserMessageLike $userMessageLike
     */
    public function removeUserLike(UserMessageLike $userMessageLike)
    {
        $this->userLikes->removeElement($userMessageLike);
    }

    public function getAbsolutePath()
    {
        return null === $this->filePath
            ? null
            : $this->getUploadRootDir().'/'.$this->filePath;
    }

    public function getWebPath()
    {
        return null === $this->filePath
            ? null
            : $this->getUploadDir().'/'.$this->filePath;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../public/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/images';
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }

        $image = $this->getFile()->getPathname();

        $uid = uniqid('', true);
        $ext = $this->getFile()->getClientOriginalExtension();

        $filePath = sprintf("%s.%s", $uid, $ext);

        $imageDetails = getimagesize($image);
        $orientation = null;

        // get the image type
        switch ($imageDetails['mime']) {
            case 'image/pjpeg':
            case 'image/jpeg':
            case 'image/jpg':
                $exif = exif_read_data($image);
                if (isset($exif['Orientation'])) {
                    $orientation = $exif['Orientation']; // yes, that's a capital O
                }
                $originalImage = imagecreatefromjpeg($image);
                break;
            case 'image/png':
                $originalImage = imagecreatefrompng($image);
                break;
            case 'image/gif':
                $originalImage = imagecreatefromgif($image);
                break;
            case 'image/webp':
                $originalImage = imagecreatefromwebp($image);
                break;
            default:
                throw new \Exception("This file doesn't appear to be an image");
        }

        /*
            exif orientation codes from http://sylvana.net/jpegcrop/exif_orientation.html
            Value    0th Row      0th Column
            1        top          left side (normal)
            2        top          right side
            3        bottom       right side (rotated 180)
            4        bottom       left side
            5        left side    top
            6        right side   top (rotated 90)
            7        right side   bottom
            8        left side    bottom (rotated -90)
            Can't be bothered dealing with mirror images so will ignore
            (no flip functionality in GD so pixel by pixel ball-ache required to implement)
         */
        switch ($orientation) {
            case 3:
                $originalImage = imagerotate($originalImage, 180, 0);
                break;
            case 6:
                $originalImage = imagerotate($originalImage, -90, 0);
                break;
            case 8:
                $originalImage = imagerotate($originalImage, 90, 0);
                break;
        }

        $width = imagesx($originalImage);
        $height = imagesy($originalImage);

        if ($width > self::MAX_IMAGE_SIZE || $height > self::MAX_IMAGE_SIZE) {
            $scale = self::MAX_IMAGE_SIZE / max([$width, $height]);

            $newImage = imagecreatetruecolor(round($width * $scale), round($height* $scale));
            imagealphablending($newImage, false);
            imagecopyresampled(
                $newImage,
                $originalImage,
                0,
                0,
                0,
                0,
                round($width * $scale),
                round($height * $scale),
                $width,
                $height
            );
        } else {
            $newImage = $originalImage;
        }


        // save it
        switch ($imageDetails['mime']) {
            case 'image/pjpeg':
            case 'image/jpeg':
            case 'image/jpg':
                imagejpeg($newImage, $image, 75);
                break;
            case 'image/png':
                imagepng($newImage, $image);
                break;
            case 'image/gif':
                imagegif($newImage, $image);
                break;
            case 'image/webp':
                imagewebp($newImage, $image, 75);
                break;
        }

        //$this->setFile($newImage);

        // move takes the target directory and then the
        // target filename to move to
        $this->getFile()->move(
            $this->getUploadRootDir(),
            $filePath
        );

        // set the path property to the filename where you've saved the file
        $this->filePath = $filePath;

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }

    /**
     * @return mixed
     */
    public function getLikeCount()
    {
        return $this->likeCount ? $this->likeCount : 0;
    }

    /**
     * @param mixed $likeCount
     * @return self
     */
    public function setLikeCount($likeCount)
    {
        $this->likeCount = $likeCount;

        return $this;
    }

    /**
     * Gets whether this message has been liked by the user
     *
     * @param User $user
     * @return bool
     */
    public function getIsLikedByUser(User $user)
    {
        foreach ($this->getUserLikes() as $userLike) {
            if ($userLike->getUser() === $user) {
                return true;
            }
        }

        return false;
    }
}
