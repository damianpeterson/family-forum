<?php

namespace App\Entity;

use App\Entity\Common\CreatableEntity;
use App\Entity\Common\DeletableEntity;
use App\Entity\Common\IdentifiableEntity;
use App\Entity\Common\UpdatableEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable
{
    use IdentifiableEntity;
    use CreatableEntity;
    use UpdatableEntity;
    use DeletableEntity;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=255)
     */
    private $salt;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255)
     */
    private $username;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="born_at", type="date", nullable=true)
     */
    private $bornAt;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text", nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_home", type="string", length=255, nullable=true)
     */
    private $phoneHome;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_work", type="string", length=255, nullable=true)
     */
    private $phoneWork;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_mobile", type="string", length=255, nullable=true)
     */
    private $phoneMobile;

    /**
     * @var Message[] | ArrayCollection $messages
     *
     * @ORM\OneToMany(targetEntity="Message", mappedBy="user")
     */
    private $messages;

    /**
     * @var UserMessageLike[] | ArrayCollection $messageLikes
     *
     * @ORM\OneToMany(targetEntity="UserMessageLike", mappedBy="user")
     */
    private $messageLikes;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=20)
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="timezone", type="string", length=255)
     */
    private $timezone;

    /**
     * @var \DateTime $visitedAt
     *
     * @ORM\Column(name="visited_at", type="datetime", nullable=true)
     */
    private $visitedAt;

    /**
     * @var string $messageCount
     *
     * @ORM\Column(name="message_count", type="integer", nullable=true)
     */
    private $messageCount;

    public function __construct()
    {
        $this->messages = new ArrayCollection();
        $this->messageLikes = new ArrayCollection();
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set bornAt
     *
     * @param \DateTime $bornAt
     * @return User
     */
    public function setBornAt($bornAt)
    {
        $this->bornAt = $bornAt;

        return $this;
    }

    /**
     * Get bornAt
     *
     * @return \DateTime 
     */
    public function getBornAt()
    {
        return $this->bornAt;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set phoneHome
     *
     * @param string $phoneHome
     * @return User
     */
    public function setPhoneHome($phoneHome)
    {
        $this->phoneHome = $phoneHome;

        return $this;
    }

    /**
     * Get phoneHome
     *
     * @return string
     */
    public function getPhoneHome()
    {
        return $this->phoneHome;
    }

    /**
     * Set phoneWork
     *
     * @param string $phoneWork
     * @return User
     */
    public function setPhoneWork($phoneWork)
    {
        $this->phoneWork = $phoneWork;

        return $this;
    }

    /**
     * Get phoneWork
     *
     * @return string
     */
    public function getPhoneWork()
    {
        return $this->phoneWork;
    }

    /**
     * Set phoneMobile
     *
     * @param string $phoneMobile
     * @return User
     */
    public function setPhoneMobile($phoneMobile)
    {
        $this->phoneMobile = $phoneMobile;

        return $this;
    }

    /**
     * Get phoneMobile
     *
     * @return string
     */
    public function getPhoneMobile()
    {
        return $this->phoneMobile;
    }

    /**
     * Get messages for this user which haven't been deleted
     *
     * @return Message[]|ArrayCollection
     */
    public function getMessages()
    {
        $criteria = Criteria::create();
        $criteria->andWhere(Criteria::expr()->isNull('deletedAt'));
        $criteria->orderBy(['createdAt' => Criteria::DESC]);

        return $this->messages->matching($criteria);
    }

    /**
     * Get all messages for this user (even deleted ones)
     *
     * @return Message[]|ArrayCollection
     */
    public function getAllMessages()
    {
        return $this->messages;
    }

    /**
     * Add a message for this user
     *
     * @param Message $message
     * @return self
     */
    public function addMessage(Message $message)
    {
        $this->messages[] = $message;

        return $this;
    }

    /**
     * Remove a message for a user
     *
     * @param Message $message
     */
    public function removeMessage(Message $message)
    {
        $this->messages->removeElement($message);
    }

    /**
     * Get all messages likes for this user
     *
     * @return UserMessageLike[]|ArrayCollection
     */
    public function getMessageLikes()
    {
        return $this->messageLikes;
    }

    /**
     * Add a message like for this user
     *
     * @param UserMessageLike $userMessageLike
     * @return self
     */
    public function addMessageLike(UserMessageLike $userMessageLike)
    {
        $this->messageLikes[] = $userMessageLike;

        return $this;
    }

    /**
     * Remove a message like for a user
     *
     * @param UserMessageLike $userMessageLike
     */
    public function removeMessageLike(UserMessageLike $userMessageLike)
    {
        $this->messageLikes->removeElement($userMessageLike);
    }

    /**
     * Get whether or not a user has liked a message
     *
     * @param Message $message
     * @return bool
     */
    public function hasLikedMessage(Message $message)
    {
        $criteria = Criteria::create();
        $criteria->andWhere(Criteria::expr()->eq('message', $message));

        $likes = $this->messageLikes->matching($criteria);

        return count($likes) > 0 ? true : false;
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return Role[] The user roles
     */
    public function getRoles()
    {
        return array('ROLE_USER');
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        return true;
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }

    /**
     * @param $color
     * @return self
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get a colour based on username
     *
     * @return string
     */
    public function getColor()
    {
        $color = $this->color;

        if (!$color) {
            $color = '#' . str_pad(dechex(crc32($this->getUsername()) / 256), 6, '000000', STR_PAD_LEFT);
        }

        return $color;
    }

    /**
     * @param $timezone
     * @return self
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * @return string
     */
    public function getTimezone()
    {
        $timezone = $this->timezone;

        if (!$timezone) {
            $timezone = 'Pacific/Auckland';
        }

        return $timezone;
    }

    /**
     * Get an object representation of the user's timezone
     * @return \DateTimeZone
     */
    public function getTimezoneObject()
    {
        return new \DateTimeZone($this->getTimezone());
    }

    /**
     * Set visitedAt
     *
     * @param \DateTime $visitedAt
     * @return self
     */
    public function setVisitedAt($visitedAt)
    {
        $this->visitedAt = $visitedAt;

        return $this;
    }

    /**
     * Get visitedAt
     *
     * @return \DateTime
     */
    public function getVisitedAt()
    {
        return $this->visitedAt;
    }

    /**
     * @return mixed
     */
    public function getMessageCount()
    {
        return $this->messageCount ? $this->messageCount : 0;
    }

    /**
     * @param mixed $messageCount
     * @return self
     */
    public function setMessageCount($messageCount)
    {
        $this->messageCount = $messageCount;

        return $this;
    }
}
