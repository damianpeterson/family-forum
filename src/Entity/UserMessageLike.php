<?php

namespace App\Entity;

use App\Entity\Common\CreatableEntity;
use App\Entity\Common\IdentifiableEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Message
 *
 * @ORM\Table(name="user_message_likes")
 * @ORM\Entity(repositoryClass="App\Repository\MessageRepository")
 */
class UserMessageLike
{
    use IdentifiableEntity;
    use CreatableEntity;

    /**
     * @var User $user
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="messageLikes")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var Message $message
     *
     * @ORM\ManyToOne(targetEntity="Message", inversedBy="userLikes")
     * @ORM\JoinColumn(name="message_id", referencedColumnName="id")
     */
    private $message;

    public function __construct()
    {

    }

    /**
     * Set user
     *
     * @param User $user
     * @return self
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set message
     *
     * @param Message $message
     * @return self
     */
    public function setMessage(Message $message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return Message
     */
    public function getMessage()
    {
        return $this->message;
    }

}
