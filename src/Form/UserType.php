<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{

    public function __construct()
    {

    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'username',
                EmailType::class,
                [
                    'required' => false,
                    'disabled' => true
                ]
            )
            ->add(
                'email',
                EmailType::class,
                [
                    'required' => false
                ]
            )
            ->add(
                'address',
                TextareaType::class,
                [
                    'required' => false
                ]
            )
            ->add(
                'phoneHome',
                TelType::class,
                [
                    'required' => false
                ]
            )
            ->add(
                'phoneMobile',
                TelType::class,
                [
                    'required' => false
                ]
            )
            ->add(
                'phoneWork',
                TelType::class,
                [
                    'required' => false
                ]
            )
            ->add(
                'color',
                ColorType::class,
                [
                    'required' => false,
                    'attr'     => [
                        'class' => 'color-picker'
                    ]
                ]
            )
            ->add(
                'password',
                PasswordType::class,
                [
                    'mapped' => false,
                    'required' => false
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            ['data_class' => 'App\Entity\User']
        );
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'user';
    }
}