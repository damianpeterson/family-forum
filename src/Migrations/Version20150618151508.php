<?php

namespace App\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20150618151508 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\DBALException
     */
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users CHANGE born_at born_at DATETIME DEFAULT NULL, CHANGE address address LONGTEXT DEFAULT NULL, CHANGE phone_home phone_home VARCHAR(255) DEFAULT NULL, CHANGE phone_work phone_work VARCHAR(255) DEFAULT NULL, CHANGE phone_mobile phone_mobile VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE messages CHANGE body body LONGTEXT DEFAULT NULL, CHANGE file_path file_path VARCHAR(255) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\DBALException
     */
    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE messages CHANGE body body LONGTEXT NOT NULL, CHANGE file_path file_path VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE users CHANGE born_at born_at DATETIME NOT NULL, CHANGE address address LONGTEXT NOT NULL, CHANGE phone_home phone_home VARCHAR(255) NOT NULL, CHANGE phone_work phone_work VARCHAR(255) NOT NULL, CHANGE phone_mobile phone_mobile VARCHAR(255) NOT NULL');
    }
}
