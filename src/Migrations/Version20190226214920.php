<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190226214920 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE messages SET body = REPLACE(body, "<img src=\'/images/biggrin.gif\' />", \'😁\') WHERE INSTR(body, "<img src=\'/images/biggrin.gif\' />") > 0;');
        $this->addSql('UPDATE messages SET body = REPLACE(body, "<img src=\'/images/smile.gif\' />", \'☺️\') WHERE INSTR(body, "<img src=\'/images/smile.gif\' />") > 0;');
        $this->addSql('UPDATE messages SET body = REPLACE(body, "<img src=\'/images/kiss.gif\' />", \'😘\') WHERE INSTR(body, "<img src=\'/images/kiss.gif\' />") > 0;');
        $this->addSql('UPDATE messages SET body = REPLACE(body, "<img src=\'/images/wink.gif\' />", \'😉\') WHERE INSTR(body, "<img src=\'/images/wink.gif\' />") > 0;');
        $this->addSql('UPDATE messages SET body = REPLACE(body, "<img src=\'/images/power.gif\' />", \'😏\') WHERE INSTR(body, "<img src=\'/images/power.gif\' />") > 0;');
        $this->addSql('UPDATE messages SET body = REPLACE(body, "<img src=\'/images/sad.gif\' />", \'😢\') WHERE INSTR(body, "<img src=\'/images/sad.gif\' />") > 0;');
        $this->addSql('UPDATE messages SET body = REPLACE(body, "<img src=\'/images/razz.gif\' />", \'😋\') WHERE INSTR(body, "<img src=\'/images/razz.gif\' />") > 0;');

    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE messages SET body = REPLACE(body, \'😁\', "<img src=\'/images/biggrin.gif\' />") WHERE INSTR(body, \'😁\') > 0;');
        $this->addSql('UPDATE messages SET body = REPLACE(body, \'☺️\', "<img src=\'/images/smile.gif\' />") WHERE INSTR(body, \'☺️\') > 0;');
        $this->addSql('UPDATE messages SET body = REPLACE(body, \'😘\', "<img src=\'/images/kiss.gif\' />") WHERE INSTR(body, \'😘\') > 0;');
        $this->addSql('UPDATE messages SET body = REPLACE(body, \'😉\', "<img src=\'/images/wink.gif\' />") WHERE INSTR(body, \'😉\') > 0;');
        $this->addSql('UPDATE messages SET body = REPLACE(body, \'😏\', "<img src=\'/images/power.gif\' />") WHERE INSTR(body, \'😏\') > 0;');
        $this->addSql('UPDATE messages SET body = REPLACE(body, \'😢\', "<img src=\'/images/sad.gif\' />") WHERE INSTR(body, \'😢\') > 0;');
        $this->addSql('UPDATE messages SET body = REPLACE(body, \'😋\', "<img src=\'/images/razz.gif\' />") WHERE INSTR(body, \'😋\') > 0;');
    }
}
