<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200926201056 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX idx_user_message_likes_IDX_58E21CC6537A1329');
        $this->addSql('DROP INDEX idx_user_message_likes_IDX_58E21CC6A76ED395');
        $this->addSql('CREATE TEMPORARY TABLE __temp__user_message_likes AS SELECT id, user_id, message_id, created_at FROM user_message_likes');
        $this->addSql('DROP TABLE user_message_likes');
        $this->addSql('CREATE TABLE user_message_likes (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_id INTEGER DEFAULT NULL, message_id INTEGER DEFAULT NULL, created_at DATETIME NOT NULL, CONSTRAINT FK_58E21CC6A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_58E21CC6537A1329 FOREIGN KEY (message_id) REFERENCES messages (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO user_message_likes (id, user_id, message_id, created_at) SELECT id, user_id, message_id, created_at FROM __temp__user_message_likes');
        $this->addSql('DROP TABLE __temp__user_message_likes');
        $this->addSql('CREATE INDEX IDX_58E21CC6A76ED395 ON user_message_likes (user_id)');
        $this->addSql('CREATE INDEX IDX_58E21CC6537A1329 ON user_message_likes (message_id)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__users AS SELECT id, email, username, born_at, created_at, updated_at, deleted_at, salt, password, address, phone_home, phone_work, phone_mobile, color, timezone, visited_at, message_count FROM users');
        $this->addSql('DROP TABLE users');
        $this->addSql('CREATE TABLE users (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, email VARCHAR(255) DEFAULT NULL COLLATE BINARY, born_at DATE DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, salt VARCHAR(255) NOT NULL COLLATE BINARY, password VARCHAR(255) NOT NULL COLLATE BINARY, address CLOB DEFAULT NULL COLLATE NOCASE, phone_home VARCHAR(255) DEFAULT NULL COLLATE BINARY, phone_work VARCHAR(255) DEFAULT NULL COLLATE BINARY, phone_mobile VARCHAR(255) DEFAULT NULL COLLATE BINARY, color VARCHAR(20) NOT NULL COLLATE BINARY, timezone VARCHAR(255) NOT NULL COLLATE BINARY, visited_at DATETIME DEFAULT NULL, message_count INTEGER DEFAULT NULL, username VARCHAR(255) NOT NULL COLLATE NOCASE)');
        $this->addSql('INSERT INTO users (id, email, username, born_at, created_at, updated_at, deleted_at, salt, password, address, phone_home, phone_work, phone_mobile, color, timezone, visited_at, message_count) SELECT id, email, username, born_at, created_at, updated_at, deleted_at, salt, password, address, phone_home, phone_work, phone_mobile, color, timezone, visited_at, message_count FROM __temp__users');
        $this->addSql('DROP TABLE __temp__users');
        $this->addSql('DROP INDEX messages_created_at_index');
        $this->addSql('CREATE TEMPORARY TABLE __temp__messages AS SELECT id, user_id, body, file_path, created_at, updated_at, deleted_at, like_count FROM messages');
        $this->addSql('DROP TABLE messages');
        $this->addSql('CREATE TABLE messages (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_id INTEGER DEFAULT NULL, body CLOB DEFAULT NULL COLLATE NOCASE, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, like_count INTEGER DEFAULT NULL, file_path VARCHAR(255) DEFAULT NULL, CONSTRAINT FK_DB021E96A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO messages (id, user_id, body, file_path, created_at, updated_at, deleted_at, like_count) SELECT id, user_id, body, file_path, created_at, updated_at, deleted_at, like_count FROM __temp__messages');
        $this->addSql('DROP TABLE __temp__messages');
        $this->addSql('CREATE INDEX IDX_DB021E96A76ED395 ON messages (user_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_DB021E96A76ED395');
        $this->addSql('CREATE TEMPORARY TABLE __temp__messages AS SELECT id, user_id, body, file_path, like_count, created_at, updated_at, deleted_at FROM messages');
        $this->addSql('DROP TABLE messages');
        $this->addSql('CREATE TABLE messages (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_id INTEGER DEFAULT NULL, body CLOB DEFAULT NULL, like_count INTEGER DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, file_path VARCHAR(191) DEFAULT NULL COLLATE BINARY, is_brenter INTEGER DEFAULT NULL)');
        $this->addSql('INSERT INTO messages (id, user_id, body, file_path, like_count, created_at, updated_at, deleted_at) SELECT id, user_id, body, file_path, like_count, created_at, updated_at, deleted_at FROM __temp__messages');
        $this->addSql('DROP TABLE __temp__messages');
        $this->addSql('CREATE INDEX messages_created_at_index ON messages (created_at)');
        $this->addSql('DROP INDEX IDX_58E21CC6A76ED395');
        $this->addSql('DROP INDEX IDX_58E21CC6537A1329');
        $this->addSql('CREATE TEMPORARY TABLE __temp__user_message_likes AS SELECT id, user_id, message_id, created_at FROM user_message_likes');
        $this->addSql('DROP TABLE user_message_likes');
        $this->addSql('CREATE TABLE user_message_likes (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_id INTEGER DEFAULT NULL, message_id INTEGER DEFAULT NULL, created_at DATETIME NOT NULL)');
        $this->addSql('INSERT INTO user_message_likes (id, user_id, message_id, created_at) SELECT id, user_id, message_id, created_at FROM __temp__user_message_likes');
        $this->addSql('DROP TABLE __temp__user_message_likes');
        $this->addSql('CREATE INDEX idx_user_message_likes_IDX_58E21CC6537A1329 ON user_message_likes (message_id)');
        $this->addSql('CREATE INDEX idx_user_message_likes_IDX_58E21CC6A76ED395 ON user_message_likes (user_id)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__users AS SELECT id, email, salt, password, username, born_at, address, phone_home, phone_work, phone_mobile, color, timezone, visited_at, message_count, created_at, updated_at, deleted_at FROM users');
        $this->addSql('DROP TABLE users');
        $this->addSql('CREATE TABLE users (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, email VARCHAR(255) DEFAULT NULL, salt VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, born_at DATE DEFAULT NULL, address CLOB DEFAULT NULL, phone_home VARCHAR(255) DEFAULT NULL, phone_work VARCHAR(255) DEFAULT NULL, phone_mobile VARCHAR(255) DEFAULT NULL, color VARCHAR(20) NOT NULL, timezone VARCHAR(255) NOT NULL, visited_at DATETIME DEFAULT NULL, message_count INTEGER DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, username CLOB NOT NULL COLLATE NOCASE)');
        $this->addSql('INSERT INTO users (id, email, salt, password, username, born_at, address, phone_home, phone_work, phone_mobile, color, timezone, visited_at, message_count, created_at, updated_at, deleted_at) SELECT id, email, salt, password, username, born_at, address, phone_home, phone_work, phone_mobile, color, timezone, visited_at, message_count, created_at, updated_at, deleted_at FROM __temp__users');
        $this->addSql('DROP TABLE __temp__users');
    }
}
