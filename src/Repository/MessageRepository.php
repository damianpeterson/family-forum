<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Message;
use Doctrine\ORM\EntityRepository;

class MessageRepository extends EntityRepository
{
    public function getMessageCount()
    {
        $queryBuilder = $this->createQueryBuilder('m');
        $queryBuilder
            ->select('COUNT(m.id)')
            ->where('m.deletedAt IS NULL');

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    public function getMessageCountNewerThanMessage(Message $message)
    {
        $queryBuilder = $this->createQueryBuilder('m');
        $queryBuilder
            ->select('COUNT(m.id)')
            ->where('m.createdAt > :createdAt')
            ->andWhere('m.deletedAt IS NULL')
            ->setParameter('createdAt', $message->getCreatedAt());

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    public function getAllMessagesZoomed()
    {
        $queryBuilder = $this->createQueryBuilder('m');
        $queryBuilder
            ->select("m.id AS message_id, m.createdAt AS created_at, u.id AS user_id")
            ->join('m.user', 'u')
            ->where('m.deletedAt IS NULL')
            ->orderBy('m.createdAt', 'DESC');

        return $queryBuilder->getQuery()->getArrayResult();
    }

    public function getMessagesBySearchTermCount($searchTerm)
    {
        $queryBuilder = $this->createQueryBuilder('m');
        $queryBuilder
            ->select('COUNT(m.id)')
            ->where("m.body LIKE :searchTerm")
            ->andWhere('m.deletedAt IS NULL')
            ->setParameter('searchTerm', '%' . $searchTerm . '%');

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    public function getMessagesBySearchTerm($searchTerm, $page = 1)
    {
        $queryBuilder = $this->createQueryBuilder('m');
        $queryBuilder
            ->where("m.body LIKE :searchTerm")
            ->andWhere('m.deletedAt IS NULL')
            ->orderBy('m.createdAt', 'DESC')
            ->setMaxResults(Message::MESSAGES_PER_PAGE)
            ->setFirstResult(Message::MESSAGES_PER_PAGE * ($page - 1))
            ->setParameter('searchTerm', '%' . $searchTerm . '%');

        return $queryBuilder->getQuery()->getResult();
    }
}
