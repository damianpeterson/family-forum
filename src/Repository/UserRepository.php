<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * UserRepository
 */
class UserRepository extends EntityRepository implements UserProviderInterface
{
    public function loadUserByUsername($username)
    {
        $user = $this->createQueryBuilder('u')
            ->where('u.username = :username OR u.email = :email')
            ->setParameter('username', $username)
            ->setParameter('email', $username)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $user) {
            $message = sprintf(
                'Unable to find an active admin App:User object identified by "%s".',
                $username
            );
            throw new UsernameNotFoundException($message);
        }

        return $user;
    }

    public function getUsersByNextBirthday()
    {
        $queryBuilder = $this->createQueryBuilder('u')
            ->where('u.deletedAt IS NULL')
        ;

        $users = $queryBuilder->getQuery()->getResult();

        $now = new \DateTime('now');
        $nowDay = $now->format('z');

        usort($users, function (User $a, User $b) use ($nowDay) {

            $aDay = $a->getBornAt()->format('z') - $nowDay;
            $bDay = $b->getBornAt()->format('z') - $nowDay;

            if ($aDay < 0) {
                $aDay += 365;
            }

            if ($bDay < 0) {
                $bDay += 365;
            }

            if ($aDay === $bDay) {
                return 0;
            }

            return ($aDay > $bDay) ? 1 : -1;
        });

        return $users;
    }

    /**
     * Gets whether it's currently someone's birthday
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getHasBirthday()
    {
        $now = new \DateTime('now');
        $nowMonthDay = $now->format('m-d');

        $queryBuilder = $this->createQueryBuilder('u');
        $queryBuilder
            ->select('COUNT(u.id)')
            ->where('u.deletedAt IS NULL')
            ->andWhere('strftime(\'%m-%d\', u.bornAt) = :monthDay')
            ->setParameter('monthDay', $nowMonthDay)
        ;

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(
                sprintf(
                    'Instances of "%s" are not supported.',
                    $class
                )
            );
        }

        return $this->find($user->getId());
    }

    public function supportsClass($class)
    {
        return $this->getEntityName() === $class
        || is_subclass_of($class, $this->getEntityName());
    }
}
