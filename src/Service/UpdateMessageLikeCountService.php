<?php

namespace App\Service;

use App\Entity\Message;
use Doctrine\ORM\EntityManagerInterface;

class UpdateMessageLikeCountService
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function updateMessage(Message $message)
    {
        $likeCount = count($message->getUserLikes());

        if ($likeCount != $message->getLikeCount()) {
            $message->setLikeCount($likeCount);
            $this->entityManager->persist($message);
            $this->entityManager->flush();
        }
    }
}