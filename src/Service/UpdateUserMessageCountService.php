<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class UpdateUserMessageCountService
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function updateAllUsers()
    {
        $userRepository = $this->entityManager->getRepository('App:User');

        $users = $userRepository->findBy(
            ['deletedAt' => null]
        );

        foreach ($users as $user) {
            $this->updateUser($user);
        }
    }

    public function updateUser(User $user)
    {
        $messageCount = count($user->getMessages());

        if ($messageCount != $user->getMessageCount()) {
            $user->setMessageCount($messageCount);
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }
    }
}